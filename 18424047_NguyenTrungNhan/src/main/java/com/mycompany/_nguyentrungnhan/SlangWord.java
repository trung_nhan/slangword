/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany._nguyentrungnhan;

import java.io.Serializable;

/**
 * SlangWord
 * @author Trung Nhan
 */
public class SlangWord implements Serializable {

    String Key;
    String Value;

    /**
     * init slang word SlangWord
     *
     * @param Key
     * @param Value
     */
    public SlangWord(String Key, String Value) {
        this.Key = Key;
        this.Value = Value;
    }

    /**
     * getKey
     * @return Key
     */
    public String getKey() {
        return Key;
    }

    /**
     * setKey
     * @param Key 
     */
    public void setKey(String Key) {
        this.Key = Key;
    }

    /**
     * getValue
     * @return Value
     */
    public String getValue() {
        return Value;
    }

    /**
     * setValue
     * @param Value 
     */
    public void setValue(String Value) {
        this.Value = Value;
    }

    /**
     * toString
     * @return Key, Value
     */
    @Override
    public String toString() {
        return "Tu: " + Key + "\nNghia: " + Value;
    }

    /**
     * display
     */
    public void display() {
        System.out.println(toString());
    }
}
