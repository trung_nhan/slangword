/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany._nguyentrungnhan;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * Features
 *
 * @author Trung Nhan
 */
public class Features {

    public static List<SlangWord> list = new ArrayList<>();
    public static HashMap<String, ArrayList<String>> hashmap = new HashMap<String, ArrayList<String>>();
    public static ArrayList<String> history = new ArrayList<>();

    /**
     * ReadFile feature
     *
     * @param list
     * @param hashmap
     * @throws IOException
     */
    public static void ReadFile(List<SlangWord> list, HashMap<String, ArrayList<String>> hashmap) throws IOException {
        String line;
        try {
            BufferedReader csvReader = new BufferedReader(new FileReader("slang.txt"));
            while ((line = csvReader.readLine()) != null) {
                String[] data = line.split("`");
                if (data.length == 2) {

                    SlangWord SW = new SlangWord(data[0], data[1]);
                    Collections.addAll(list, SW);
                }

            }
        } catch (IOException e) {

        }

        // add key and value into hash map
        for (int i = 0; i < list.size(); i++) {
            ArrayList<String> values = new ArrayList<>();
            String[] value = list.get(i).getValue().split("\\| ");
            values.addAll(Arrays.asList(value));
            hashmap.put(list.get(i).getKey(), values);
        }
        // read file history
        String row;
        try {
            BufferedReader csvReader = new BufferedReader(new FileReader("history.txt"));
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split("/n");
                history.add(data[0]);
            }
        } catch (IOException e) {

        }

    }

    /**
     * SearchSlangWord
     *
     * @param list
     * @param hashmap
     * @throws IOException
     */
    public static void SearchSlangWord(List<SlangWord> list, HashMap<String, ArrayList<String>> hashmap) throws IOException {
        System.out.println("Nhap tu (Slang Word) ban can tim: ");
        Scanner input = new Scanner(System.in);
        String key = input.nextLine();

        history.add(key);
        Path path = Paths.get("history.txt");
        try ( BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8, StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
            writer.write(key + System.lineSeparator());
        } catch (IOException e) {
        }

        if (hashmap.get(key) == null) {
            System.out.println("==>Khong Co Tu Ban Can Tim.");
            return;
        }
        System.out.println("==>>Tu nay co " + hashmap.get(key).size() + " nghia <<==");
        for (int i = 0; i < hashmap.get(key).size(); i++) {
            System.out.println("Nghia Thu " + (i + 1) + ":");
            System.out.println(hashmap.get(key).get(i));
        }

    }

    /**
     * SearchDefinition
     *
     * @param list
     * @param hashmap
     * @throws IOException
     */
    public static void SearchDefinition(List<SlangWord> list, HashMap<String, ArrayList<String>> hashmap) throws IOException {
        System.out.println("Nhap tu (definition) ban can tim: ");
        Scanner input = new Scanner(System.in);
        String Value = input.nextLine();
        history.add(Value);
        int count = 1;
        System.out.println("==>>Nhung tu (definition) co chua " + Value + " la:");
        for (String i : hashmap.keySet()) {
            for (int j = 0; j < hashmap.get(i).size(); j++) {
                if (hashmap.get(i).get(j).contains(Value)) {
                    System.out.println("Tu thu " + count);
                    System.out.println(i + "   " + hashmap.get(i).get(j));
                    count++;
                }

            }
        }
        if (count == 1) {
            System.out.println("==>Khong Co Tu Ban Can Tim.");
        }
        Path path = Paths.get("history.txt");
        try ( BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8, StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
            writer.write(Value + System.lineSeparator());
        } catch (IOException e) {
        }

    }

    /**
     * DisplayHistory
     *
     * @param history
     * @throws IOException
     */
    public static void DisplayHistory(ArrayList<String> history) throws IOException {
        for (int i = 0; i < history.size(); i++) {
            System.out.println(history.get(i));
        }
    }

    /**
     * RevertFile
     *
     * @param fileRoot
     * @param file
     * @throws IOException
     */
    public static void RevertFile(File fileRoot, File file) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(fileRoot);
            os = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }

    /**
     * RandomSlangWord
     *
     * @throws IOException
     */
    public static void RandomSlangWord() throws IOException {
        Random rand = new Random();
        int range = hashmap.size();
        int randomNumber = rand.nextInt(range);
        System.out.println(list.get(randomNumber).toString());
    }

    /**
     * QuizzSlangWord
     */
    public static void QuizzSlangWord() {
        Random rand = new Random();
        int range = hashmap.size();
        int songaunhien = rand.nextInt(range);
        System.out.println("Tu " + list.get(songaunhien).getKey() + " nghia la gi?");
        ArrayList<String> answer = new ArrayList<>();
        String correctAnswer = hashmap.get(list.get(songaunhien).getKey()).get(0);
        answer.add(correctAnswer);
        for (int i = 0; i < 3; i++) {
            int number = rand.nextInt(range);
            String b = hashmap.get(list.get(number).getKey()).get(0);
            answer.add(b);
        }
        Collections.sort(answer);
        System.out.println("1. " + answer.get(0) + "         2. " + answer.get(1));
        System.out.println("3. " + answer.get(2) + "         4. " + answer.get(3));
        System.out.println("Moi ban chon dap an: ");
        Scanner input = new Scanner(System.in);
        int selectAnswer = input.nextInt();
        if (answer.get(selectAnswer - 1).equals(correctAnswer)) {
            System.out.println("Chuc mung ban chon dung!");
            return;
        }
        System.out.println("Ban da chon sai, dap an la: " + correctAnswer);
    }

    /**
     * QuizzDefinition
     */
    public static void QuizzDefinition() {
        Random rand = new Random();
        int range = hashmap.size();
        int numberRandom = rand.nextInt(range);
        String[] mean = list.get(numberRandom).getValue().split("\\| ");
        String result = list.get(numberRandom).getKey();
        int meanRandom = rand.nextInt(mean.length);
        System.out.println(meanRandom);
        System.out.println("Tu viet Tat cua nghia " + mean[meanRandom] + " la gi?");
        ArrayList<String> answer = new ArrayList<>();
        answer.add(result);
        for (int i = 0; i < 3; i++) {
            int so = rand.nextInt(range);
            String b = list.get(so).getKey();
            answer.add(b);
        }
        Collections.sort(answer);
        System.out.println("1. " + answer.get(0) + "         2. " + answer.get(1));
        System.out.println("3. " + answer.get(2) + "         4. " + answer.get(3));
        System.out.println("Moi ban chon dap an: ");
        Scanner input = new Scanner(System.in);
        int chon = input.nextInt();
        if (answer.get(chon - 1).equals(result)) {
            System.out.println("Chuc mung ban chon dung!");
            return;
        }
        System.out.println("Ban da chon sai, dap an la: " + result);
    }

    /**
     * AddNewSlangWord
     *
     * @param list
     * @param hashmap
     * @throws IOException
     */
    public static void AddNewSlangWord(List<SlangWord> list, HashMap<String, ArrayList<String>> hashmap) throws IOException {
        System.out.println("Moi ban nhap tu (Slang Word): ");
        Scanner input = new Scanner(System.in);
        String value;
        String key = input.nextLine();
        // trung
        if (hashmap.get(key) != null) {
            System.out.println("Slang Word da trung");
            System.out.println("=>1. Ghi de");
            System.out.println("=>2. Sao chep");
            input = new Scanner(System.in);
            int chon = input.nextInt();
            System.out.println("Moi ban nhap nghia cua " + key);
            input = new Scanner(System.in);
            value = input.nextLine();
            if (chon == 1) {
                File tempFile = new File("tempslang.txt");
                String line;
                try ( RandomAccessFile raf = new RandomAccessFile(new File("slang.txt"), "rw");  RandomAccessFile tempraf = new RandomAccessFile(tempFile, "rw")) {
                    raf.seek(0);
                    while (raf.getFilePointer() < raf.length()) {
                        line = raf.readLine();
                        String[] data = line.split("`");
                        if (data.length == 2) {
                            if (data[0].equals(key)) {
                                line = key + "`" + data[1] + "| " + value;
                            }
                            tempraf.writeBytes(line + System.lineSeparator());
                        }

                    }
                    RevertFile(new File("tempslang.txt"), new File("slang.txt"));
                    ReadFile(list, hashmap);
                    raf.setLength(tempraf.length());
                }
                tempFile.delete();

                System.out.println("Them moi thanh cong. ");
            }
            if (chon == 2) {
                SlangWord SW = new SlangWord(key, value);
                Collections.addAll(list, SW);
                ArrayList<String> word = new ArrayList<>();
                word.add(value);
                hashmap.put(key, word);
                Path path = Paths.get("slang.txt");
                String text = key + "`" + value;
                try ( BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8, StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
                    writer.write(text);
                } catch (IOException e) {
                }
                System.out.println("Them moi thanh cong. ");
            }
        }
        // khong trung
        if (hashmap.get(key) == null) {
            System.out.println("Moi ban nhap nghia cua " + key);
            input = new Scanner(System.in);
            value = input.nextLine();
            SlangWord SW = new SlangWord(key, value);
            Collections.addAll(list, SW);
            ArrayList<String> word = new ArrayList<>();
            word.add(value);
            hashmap.put(key, word);
            Path path = Paths.get("slang.txt");
            String text = key + "`" + value;
            try ( BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8, StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
                writer.write(text);
            } catch (IOException e) {
            }
            System.out.println("Them moi thanh cong. ");
        }

    }

    /**
     * DeleteSlangWord
     *
     * @param list
     * @param hashmap
     * @throws IOException
     */
    public static void DeleteSlangWord(List<SlangWord> list, HashMap<String, ArrayList<String>> hashmap) throws IOException {
        System.out.println("Ban nhap Slang Word can xoa");
        Scanner input = new Scanner(System.in);
        String key = input.nextLine();
        if (hashmap.get(key) == null) {
            System.out.println("=>>Khong co tu " + key + " ban can xoa");
            return;
        }
        File tempFile = new File("tempslang.txt");
        String line;
        RandomAccessFile raf = new RandomAccessFile(new File("slang.txt"), "rw");
        RandomAccessFile tempraf = new RandomAccessFile(tempFile, "rw");
        raf.seek(0);
        while (raf.getFilePointer() < raf.length()) {
            line = raf.readLine();
            String[] data = line.split("`");
            if (data.length == 2) {
                if (data[0].equals(key)) {
                    hashmap.remove(key);
                    list.remove(key);
                    continue;
                }
                tempraf.writeBytes(line + System.lineSeparator());
            }

        }
        RevertFile(new File("tempslang.txt"), new File("slang.txt"));
        raf.setLength(tempraf.length());
        tempraf.close();
        raf.close();
        tempFile.delete();
        System.out.println("Xoa thanh cong tu " + key);
    }

    public static void UpdateSlangWord(List<SlangWord> list, HashMap<String, ArrayList<String>> hashmap) throws IOException {
        System.out.println("Ban nhap Slang Word can cap nhat");
        Scanner input = new Scanner(System.in);
        String key = input.nextLine();
        if (hashmap.get(key) == null) {
            System.out.println("=>>Khong co tu ban can cap nhat");
            return;
        }
        System.out.println("Moi ban nhap y nghia cua tu " + key);
        input = new Scanner(System.in);
        String value = input.nextLine();
        File tempFile = new File("tempslang.txt");
        String line;
        RandomAccessFile raf = new RandomAccessFile(new File("slang.txt"), "rw");
        RandomAccessFile tempraf = new RandomAccessFile(tempFile, "rw");
        raf.seek(0);
        while (raf.getFilePointer() < raf.length()) {
            line = raf.readLine();
            String[] data = line.split("`");
            if (data.length == 2) {
                if (data[0].equals(key)) {
                    line = key + "`" + value;
                }
                tempraf.writeBytes(line + System.lineSeparator());
            }

        }
        RevertFile(new File("tempslang.txt"), new File("slang.txt"));
        ReadFile(list, hashmap);
        raf.setLength(tempraf.length());
        tempraf.close();
        raf.close();
        tempFile.delete();

        System.out.println("Cap nhat thanh cong tu " + key);

    }

    /**
     * InputMenu features
     *
     * @throws IOException
     */
    public static void InputMenu() throws IOException {
        int selectMenu;
        while (true) {
            System.out.println("===============Menu-Slang-Word===============");
            System.out.println("1. Tim kiem theo slang word.");
            System.out.println("2. Tim kiem theo definition.");
            System.out.println("3. Hien thi lich su.");
            System.out.println("4. Them moi 1 slang word.");
            System.out.println("5. Chinh sua 1 slang word.");
            System.out.println("6. Xoa 1 slang word.");
            System.out.println("7. Reset danh sach slang word ve goc.");
            System.out.println("8. Chon ngau nhien 1 slang word.");
            System.out.println("9. Cau do 1(theo slang word).");
            System.out.println("10. Cau do 2(theo definition).");
            System.out.println("11. Thoat Chuong Trinh.");
            System.out.println("==> Chon chuc nang: ");
            Scanner input = new Scanner(System.in);
            selectMenu = input.nextInt();
            switch (selectMenu) {
                case 1:
                    SearchSlangWord(list, hashmap);
                    break;
                case 2:
                    SearchDefinition(list, hashmap);
                    break;
                case 3:
                    DisplayHistory(history);
                    break;
                case 4:
                    AddNewSlangWord(list, hashmap);
                    break;
                case 5:
                    UpdateSlangWord(list, hashmap);
                    break;
                case 6:
                    DeleteSlangWord(list, hashmap);
                    break;
                case 7:
                    RevertFile(new File("slangroot.txt"), new File("slang.txt"));
                    ReadFile(list, hashmap);
                    System.out.println("Ban da khoi phuc file goc thanh cong");
                    break;
                case 8:
                    RandomSlangWord();
                    break;
                case 9:
                    QuizzSlangWord();
                    break;
                case 10:
                    QuizzDefinition();
                    break;
                case 11:
                    return;
                default:
                    System.out.println("Chuc nang khong co, moi ban chon lai.");
                    break;

            }
        }
    }

}
