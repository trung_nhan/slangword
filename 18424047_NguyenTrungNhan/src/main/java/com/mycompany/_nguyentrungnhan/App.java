/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany._nguyentrungnhan;

import java.io.IOException;

/**
 * App
 * @author Trung Nhan
 */
public class App {

    public static void main(String[] args) throws IOException {
        //  Features F = new Features();
        // read file
        Features.ReadFile(Features.list, Features.hashmap);
        // features input menu
         Features.InputMenu();
    }
}
